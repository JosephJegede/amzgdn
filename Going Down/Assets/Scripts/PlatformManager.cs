﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    public GameObject[] _platformTypes;

    private GameObject[] _platformPrefabs;

    [SerializeField]
    private int _amountOfPlatforms;

    public Transform _platStartPos;

    public Transform _platRestartPos;

    public Transform _platResetPos;

    public float _yMinOffset, _yMaxOffset, _xMinOffset, _xMaxOffset;

    private void Start()
    {
        //Instantiate the amount of platforms
        //and thier types required for the stage
        for( int i = 0; i < _amountOfPlatforms; i++ )
        {
            _platStartPos.transform.position = new Vector3(Random.Range(_xMinOffset,_xMaxOffset)
            ,_platStartPos.position.y - Random.Range(_yMinOffset,_yMaxOffset) ,_platStartPos.position.z);

            Instantiate(_platformTypes[Random.Range(0,_platformTypes.Length)].gameObject,_platStartPos.position, Quaternion.identity);
        }
    }
}
