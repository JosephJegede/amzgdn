﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    [SerializeField]
    private PlatformManager _platformManager;

    public float _moveSpeed, _resetOffset;

    public float _yMinOffset, _yMaxOffset, _xMinOffset, _xMaxOffset;

    private Vector3 _platStartPos;

    private Vector3 _platRestartPos;

    private Vector3 _platResetPos;

    //Called at the start of this instance
    private void Start()
    {
        //Search for the platform manager game object in the scene
        _platformManager = FindObjectOfType<PlatformManager>();

        _platStartPos = _platformManager._platStartPos.position;

        _platRestartPos = _platformManager._platRestartPos.position;

        _platResetPos = _platformManager._platResetPos.position;

        _yMinOffset = _platformManager._yMinOffset;

        _yMaxOffset = _platformManager._yMaxOffset;

        _xMinOffset = _platformManager._xMinOffset;

        _xMaxOffset = _platformManager._xMaxOffset;
    }

    // Update is called once per frame
    private void Update ()
    {
        //Move the platforms towards a position on the screen
		transform.position = Vector3.MoveTowards(transform.position,
        new Vector3(transform.position.x,_platformManager._platResetPos.position.y, 
        transform.position.z), _moveSpeed * Time.smoothDeltaTime);

        //Reset the position of the platform
        if(transform.position.y == _platResetPos.y)
        {
            //Randomize the start position on the
            //x and y axis
            transform.position = new Vector3(Random.Range(_xMinOffset,_xMaxOffset),
                                             _platRestartPos.y - Random.Range(_yMinOffset,_yMaxOffset),
                                             transform.position.z);
        }
	}
}
