﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float _playerSpeed, _stageCutoff;

    public Vector3 _playerForce;

    public Rigidbody _rigidBody;

	// Update is called once per frame
	private void Update ()
    {
	    if(Input.GetKey("a"))
        {
            transform.Translate(Vector3.left * _playerSpeed *  Time.smoothDeltaTime);
        }
        else if (Input.GetKey("d"))
        {
            transform.Translate(Vector3.right * _playerSpeed * Time.smoothDeltaTime);
        }

        if(transform.position.y > _stageCutoff)
        {
            _rigidBody.AddForce(_playerForce);

            _rigidBody.velocity = Vector3.ClampMagnitude(_rigidBody.velocity,2);
        }
        else
        {
            _rigidBody.velocity *= Mathf.MoveTowards(1, 0, Time.smoothDeltaTime);
        }
    }
}
